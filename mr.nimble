# Package
version       = "0.1.0"
author        = "Elias Flåte Ekroll"
description   = "Map Renderer"
license       = "GPL-3.0-only"
srcDir        = "src/"
binDir        = ".oof/"
bin           = @["mr"]


# Dependencies
requires "nim >= 2.0.2"
requires "argparse >= 4.0.1"
requires "chronicles >= 0.4.2"
requires "kashae >= 0.1.1"
requires "openal"
requires "nimgl"
requires "glm"
#requires "futhark == 0.5.0" # Waiting until it builds relatively easily
#requires "netty >= 0.2.1"


# Testing
task test, "Runs the test suite":
  exec "nim c -r src/test/nim/test_helper"

task docgen, "Generates documentation":
  exec "nim doc --project --index:on --outdir:docs/ src/main.nim"
