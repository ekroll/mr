import os
import ddd
import times
import res as r
import logging
import system
import argparse
import chronicles

debug "potato"

proc main() = 
  var p = argparse.newParser():
    flag("-a", "--apple")
    flag("-b", help="Show a banana")
    option("-o", "--output", help="Output to this file")
    arg("name")
    arg("others", nargs = -1)

  try:
    var opts = p.parse(@["--apple", "-o=foo", "hi"])
    assert opts.apple == true
    assert opts.b == false
    assert opts.output == "foo"
    assert opts.name == "hi"
    assert opts.others == @[]

  except ShortCircuit as err:
    if err.flag == "argparse_help":
      echo err.help
      quit(1)

  except UsageError:
    stderr.writeLine getCurrentExceptionMsg()
    quit(1)

  let 
    settings = r.parseSettings("./settings.json")
    client = ddd.begin(settings)
    suzanne = r.parseAsset("./suzanne.json")

  var 
    universe = Universe(bodies: @[])
    frametime = 1.0 / client.fps.float

  ddd.createBody(suzanne, universe)

  # Main Loop
  while ddd.isActive(client):
    let frameStart = times.cpuTime()

    ddd.progress(universe, client, frametime)
    os.sleep(1)

    frametime = frameStart - times.cpuTime()

  ddd.finish(universe, client)


when isMainModule:
  main()
