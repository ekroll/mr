import nimgl/[opengl, glfw]
import std/options
import openal
import glm

export options, openal, opengl, glfw, glm

type
  Vertex* = object
    position*: array[0 .. 2, float32]
    normal*: array[0 .. 2, float32]
    texcoord*: array[0 .. 1, float32]


  Model* = object
    vertices*: seq[Vertex]
    vertexCount*: uint32
    #indices: seq[vec3]

    # Assigned at runtime:
    vertexArrayIndex*: Option[uint32]
    vertexBuffer*: Option[uint32]
    shaderProgram*: Option[uint32]
    vertexShader*: Option[uint32]
    fragmentShader*: Option[uint32]
    modelMatrixUniform*: Option[int32]
    viewMatrixUniform*: Option[int32]
    projectionMatrixUniform*: Option[int32]


  Transform* = object
    loc*: array[0 .. 2, float32]
    rot*: array[0 .. 2, float32]


  Asset* = ref object of RootObj
    ## Type for assets parsed from JSON
    name*: string
    mass*: float32
    trans*: Option[Transform]
    modelPath*: string
    shader*: string


  Body* = ref object of Asset
    ## Models physical objects
    id*: Option[int]
    model*: Option[Model]


  Universe* = object
    bodies*: seq[Body]


proc toModelMatrix*(transform: Transform): Mat4[float32] =
  mat4f(1)
    #[
    .rotate(
      alpha, 
      transform.rot[0],
      transform.rot[1],
      transform.rot[2]
    )
    ]#
    .translate(
      transform.loc[0],
      transform.loc[1],
      transform.loc[2]
    )


proc toVec3*[T](vec: array[0 .. 2, T]): Vec3[T] =
  return vec3(
    vec[0],
    vec[1],
    vec[2]
  )


proc toBody*(asset: Asset, model: Option[Model], id: Option[int]): Body =
  let body = Body(model: model, id: id)
  body.name = asset.name
  body.mass = asset.mass
  body.trans = asset.trans
  body.modelPath = asset.modelPath
  body.shader = asset.shader
  return body


type
  Setting* = ref object of RootObj
    ## Settings parsed from JSON
    name*: string
    near*: float64
    far*: float64
    fov*: float64
    fps*: float
    res*: array[0 .. 1, int32]
    trans*: Option[Transform]
  
  Client* = ref object of Setting
    window*: glfw.GLFWWindow


proc toClient*(setting: Setting, window: GLFWwindow): Client =
  let client = Client(window: window)
  client.name = setting.name
  client.near = setting.near
  client.far = setting.far
  client.fov = setting.fov
  client.fps = setting.fps
  client.res = setting.res
  client.trans = setting.trans
  return client