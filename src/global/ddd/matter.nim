import chronicles

import types
import res as r

from matter/mesh import uploadMeshData, EmptyMeshError
from matter/shader import attachShader


proc realize*(asset: Asset): Body =
  let data = r.loadModel(asset.modelPath)
  var body = asset.toBody(
    none(Model), 
    none(int)
  )

  if data.isSome():
    try:
      var model = data.get()
      model.uploadMeshData()
      model.attachShader(asset.shader)
      body = asset.toBody(
        some(model), # model
        none(int)    # id (assigned by continuum)
      )

    except EmptyMeshError:
      error "model has no vertices", model = asset.name

  else:
    error "failed parsing 3D model", asset = asset.name

  return body


proc destroy*(model: Model): void =
  assert model.vertexBuffer.isSome()
  assert model.vertexArrayIndex.isSome()
  assert model.vertexShader.isSome()
  assert model.fragmentShader.isSome()

  mesh.unload(
    model.vertexBuffer.get(),
    model.vertexArrayIndex.get()
  )

  shader.unload(
    model.vertexShader.get(),
    model.fragmentShader.get()
  )


proc update*(body: Body, client: Client): void =
  if body.model.isSome():
    let 
      model = body.model.get()
      requirements =
        model.shaderProgram.isSome() and
        model.vertexArrayIndex.isSome()
    
    if requirements:
      let program = model.shaderProgram.get()
      let vai = model.vertexArrayIndex.get()

      shader.dobind(program)
      shader.project(body, client)
      mesh.draw(vai, model.vertex_count)
      shader.unbind()

