import types


proc create*(universe: var Universe, body: Body): void =
  ## Adds a Object to the simulation
  let id = len(universe.bodies)
  body.id = some(id)
  universe.bodies.add(body)


proc progress*(universe: Universe, delta: float): void =
  ## progresses the universe delta time 


proc destroy*(id: int, universe: var seq[Body]) =
  ## Removes a ddd_object from the simulation
  universe.delete(id)