import std/enumerate
import std/strformat
import std/logging
import system
import types

type
  EmptyMeshError* = object of ValueError

proc uploadMeshData*(model: var Model): void =
  var vertsize: uint32

  try:
    vertsize = sizeof(model.vertices[0]).uint32
  
  except IndexDefect:
    raise EmptyMeshError.newException("model has no vertices")

  var # Prepare mutable variables with memory addresses
    vb, vai: GLuint
    vertices = model.vertices
    buffsize = model.vertex_count * vertsize

  # Prepare and bind buffer for upload
  glGenVertexArrays(1, addr vai)
  glBindVertexArray(vai)
  glGenBuffers(1, addr vb)
  glBindBuffer(GL_ARRAY_BUFFER, vb)

  # Upload raw data to the bound buffer
  glBufferData(
    GL_ARRAY_BUFFER,
    buffsize.GLsizeiptr,
    addr vertices[0].position[0], # start of buffer
    GL_STATIC_DRAW
  )

  # Prepare mutable vars for relative address calculation
  var offset: int = 0
  let attributes = [
    3, # position
    3, # normal
    2  # texcoord
  ]

  # Telling OpenGL how the raw data is structured
  for location, components in enumerate(attributes):
    glEnableVertexAttribArray(location.GLuint)
    glVertexAttribPointer(
        location.GLuint,        # layout (location = i)
        components.GLint,       # number of components
        EGL_FLOAT,              # component datatype
        false,                  # normalize? nope...
        vertsize.GLsizei,       # whole attribute size
        cast[pointer](offset)   # start address (this entry)
    )

    offset += sizeof(float) * components # increment address

  # Unbind vertex array
  glBindVertexArray(0)

  # Store OpenGL accessors in the model
  model.vertexArrayIndex = some(vai)
  model.vertexBuffer = some(vb)


proc draw*(vai: GLuint, vc: GLuint) =
    glBindVertexArray(vai)
    
    glDrawArrays(
      GL_TRIANGLES,  # draw mode
      0.GLint,       # first vertex
      vc.GLsizei     # vertex count
    )

    glBindVertexArray(0)


proc unload*(vertexBuffer: GLuint, vertexArrayIndex: GLuint) =
  var
    buffer = vertexBuffer
    vertexArray = vertexArrayIndex

  glDeleteBuffers(1, unsafeAddr buffer)
  glDisableVertexAttribArray(vertexArray)
  glDeleteVertexArrays(1, unsafeAddr vertexArray)
