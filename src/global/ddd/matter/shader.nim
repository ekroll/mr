import chronicles
import strformat
import types

proc logOpenGLStatus(id: GLuint, status: GLenum, ivproc: proc, logproc: proc): void =
  var success: GLint
  ivproc(id, status, success.addr)

  if success == GL_FALSE.ord:
    var
      length: GLint
      message: cstring
    
    ivproc(id, GL_INFO_LOG_LENGTH, length.addr)
    logproc(id, 1024, length.addr, message)
    error "OpenGL Status Message:",
      message = message
    
    writeStackTrace()
    quit(QuitFailure)
  
  else:
    debug "no opengl errors to report"


proc getLinkStatus(id: GLuint): void =
  logOpenGLStatus(id, GL_LINK_STATUS, glGetProgramiv, glGetProgramInfoLog)


proc getCompileStatus(id: GLuint): void =
  logOpenGLStatus(id, GL_COMPILE_STATUS, glGetShaderiv, glGetShaderInfoLog)


proc compileShader(shaderType: GLenum, source: var cstring): uint32 =
  let shader = glCreateShader(shaderType)
  glShaderSource(shader, 1.GLsizei, source.addr, nil)
  glCompileShader(shader)
  getCompileStatus(shader)
  shader


proc attachShader*(model: var Model, path: string): void =
  let 
    program: GLuint = glCreateProgram()
    vpath = &"{path}/vertex.glsl"
    fpath = &"{path}/fragment.glsl"

  var 
    vsauce = readFile(vpath).cstring
    fsauce = readFile(fpath).cstring

  # Vertex
  debug "compiling vertex shader", path = vpath
  let vertexShader: GLuint = compileShader(GL_vertexShader, vsauce)
  debug "attaching vertex shader to shader program"
  glAttachShader(program, vertexShader)

  # Fragment
  debug "compiling fragment shader", path = fpath
  let fragmentShader: Gluint = compileShader(GL_fragmentShader, fsauce)
  debug "attaching fragment shader to shader program"
  glAttachShader(program, fragmentShader)
  
  debug "binding shader attributes"
  glBindAttribLocation(program, 0, "a_position")
  glBindAttribLocation(program, 1, "a_normal")
  glBindAttribLocation(program, 2, "a_texture_coordinate")
  
  debug "linking shader program"
  glLinkProgram(program)
  glValidateProgram(program)
  getLinkStatus(program)

  model.shaderProgram = some(program)
  model.vertexShader = some(vertexShader)
  model.fragmentShader = some(fragmentShader)
  
  model.modelMatrixUniform = some(
    glGetUniformLocation(program, "u_model_matrix")
  )

  model.viewMatrixUniform  = some(
    glGetUniformLocation(program, "u_view_matrix")
  )

  model.projectionMatrixUniform = some(
    glGetUniformLocation(program, "u_perspective_matrix")
  )


proc dobind*(program: uint32) =
  glUseProgram(program.GLuint)


proc project*(body: Body, client: Client): void =
  ## Updates shader uniforms necessary for drawing 3D object on 2D screen
  let hasSomethingToShow = body.model.isSome()

  if hasSomethingToShow:
    let model: Model = body.model.get()

    let
      forward = vec3[float32](0.0, 0.0, 1.0)
      up = vec3[float32](0.0, 1.0, 0.0)
      ratio: float = cast[float](client.res[0])/cast[float](client.res[1])
    
    let requirements = 
      body.trans.isSome() and
      client.trans.isSome() and
      model.modelMatrixUniform.isSome() and
      model.viewMatrixUniform.isSome() and
      model.projectionMatrixUniform.isSome()
    
    if requirements:
      let
        bodyTransform = body.trans.get()
        clientTrans = client.trans.get()
        clientLoc = clientTrans.loc.toVec3()

      let
        modelMatrixUniform = model.modelMatrixUniform.get()
        viewMatrixUniform = model.viewMatrixUniform.get()
        projectionMatrixUniform = model.projectionMatrixUniform.get()
      
      var
        modelMatrixData = bodyTransform.toModelMatrix()
        viewMatrixData = lookAt(clientLoc, clientLoc + forward, up)
        projectionMatrixData = perspective(client.fov, ratio, client.near, client.far)
      
      let # adresses be retreived
        modelMatrixAddress = cast[ptr GLfloat](modelMatrixData.caddr)
        viewMatrixAddress = cast[ptr GLfloat](viewMatrixData.caddr)
        projectionMatrixAddress = cast[ptr GLfloat](projectionMatrixData.caddr)

      # Make opengl retreive data from specified adresses

      glUniformMatrix4fv(
        modelMatrixUniform, 
        1.GLsizei,
        false, 
        modelMatrixAddress
      )

      glUniformMatrix4fv(
        viewMatrixUniform.GLint, 
        1.GLsizei,
        false.GLboolean, 
        viewMatrixAddress
      )

      glUniformMatrix4fv(
        projectionMatrixUniform.GLint, 
        1.GLsizei,
        false.GLboolean, 
        projectionMatrixAddress
      )

proc unbind*() =
  glUseProgram(0)


proc unload*(vs: GLuint, fs: GLuint) =
  glDeleteShader(vs)
  glDeleteShader(fs)