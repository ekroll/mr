import nimgl/[glfw, opengl]
import chronicles
import strformat
import types

# Callbacks

proc onOpenGLMessage (
  source: GLEnum,
  typ: GLEnum,
  id: GLuint,
  severity: GLEnum,
  length: GLsizei,
  message: ptr GLchar,
  userParam: pointer
) {.stdcall.} =
  if id.int in [131169, 131185, 131218, 131204]:
    return

  var `type`: string
  case typ
    of GL_DEBUG_TYPE_ERROR:               `type` = "error"
    of GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: `type` = "deprecated behaviour"
    of GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  `type` = "undefined behaviour"
    of GL_DEBUG_TYPE_PORTABILITY:         `type` = "portability"
    of GL_DEBUG_TYPE_PERFORMANCE:         `type` = "performance"
    of GL_DEBUG_TYPE_MARKER:              `type` = "marker"
    of GL_DEBUG_TYPE_PUSH_GROUP:          `type` = "push group"
    of GL_DEBUG_TYPE_POP_GROUP:           `type` = "pop group"
    of GL_DEBUG_TYPE_OTHER:               `type` = "other"
    else:                                 `type` = "unknown"

  var sev: string
  case severity
    of GL_DEBUG_SEVERITY_NOTIFICATION:    sev = "notification"
    of GL_DEBUG_SEVERITY_HIGH:            sev = "high"
    of GL_DEBUG_SEVERITY_MEDIUM:          sev = "medium"
    of GL_DEBUG_SEVERITY_LOW:             sev = "low"
    else:                                 sev = "unknown"

  var src: string
  case source
    of GL_DEBUG_SOURCE_API:               src = "api"
    of GL_DEBUG_SOURCE_WINDOW_SYSTEM:     src = "window system"
    of GL_DEBUG_SOURCE_SHADER_COMPILER:   src = "shader compiler"
    of GL_DEBUG_SOURCE_THIRD_PARTY:       src = "third-party"
    of GL_DEBUG_SOURCE_APPLICATION:       src = "application"
    of GL_DEBUG_SOURCE_OTHER:             src = "other"
    else:                                 src = "unknown"

  debug "OpenGL Debug Message:",
    message = cast[cstring](message),
    severity = sev,
    `type` = `type`,
    source = src,
    id = id


proc onWindowResized (
  window: GLFWWindow,
  width: int32,
  height: int32
) {.cdecl.} =
  debug "window resized",
    width = width, 
    height = height


proc onFramebufferResized (
  window: GLFWwindow,
  width: GLint,
  height: GLint
) {.cdecl.} =
  glViewport(0, 0, width, height)


proc onDPIScaled (
  window: GLFWWindow,
  xscale: float32,
  yscale: float32
) {.cdecl.} =
  info "dpi changed",
    xscale = xscale,
    yscale = yscale


proc onKeyPressed (
  window: GLFWWindow,
  key: int32,
  scancode: int32,
  action: int32,
  mods: int32
) {.cdecl.} =
  if key == GLFWKey.END and action == GLFWPress:
      window.setWindowShouldClose(true)


proc prepareCallbacks(window: GLFWWindow) =
  discard window.setWindowSizeCallback(on_window_resized)
  discard window.setFramebufferSizeCallback(on_framebuffer_resized)
  discard window.setWindowContentScaleCallback(on_dpi_scaled)
  discard window.setKeyCallback(on_key_pressed)


# Methods

proc setWindowHints() =
  glfwWindowHint(GLFWOpenGLProfile, GLFW_OPENGL_COMPAT_PROFILE)
  glfwWindowHint(GLFWOpenGLForwardCompat, GLFW_TRUE)
  glfwWindowHint(GLFWOpenGLDebugContext, GLFW_TRUE)
  glfwWindowHint(GLFW_CONTEXT_NO_ERROR, GLFW_FALSE)
  glfwWindowHint(GLFWRefreshRate, GLFW_DONT_CARE)
  glfwWindowHint(GLFWDoublebuffer, GLFW_TRUE)
  glfwWindowHint(GLFWContextVersionMajor, 4)
  glfwWindowHint(GLFWContextVersionMinor, 3)
  glfwWindowHint(GLFW_DECORATED, GLFW_TRUE)
  glfwWindowHint(GLFW_MAXIMIZED, GLFW_TRUE)
  glfwWindowHint(GLFWResizable, GLFW_TRUE)
  glfwWindowHint(GLFWDepthBits, 16)
  glfwWindowHint(GLFWAlphaBits, 8)
  glfwWindowHint(GLFWGreenBits, 8)
  glfwWindowHint(GLFWBlueBits, 8)
  glfwWindowHint(GLFWredBits, 8)


proc createWindow(
  name: string, 
  resX: int, 
  resY: int
): GLFWWindow = 
  assert resX != 0
  assert resY != 0
  let window = glfwCreateWindow(
    resX.int32,
    resY.int32,
    name.cstring,
    nil, nil
  )

  window


proc openWindow*(name: string, resX: int, resY: int): Option[GLFWWindow] =
  if not glfwInit():
    fatal "failed to initialize glfw"
    return none(GLFWWindow)

  setWindowHints()

  let window = createWindow(name, resX, resY)
  if window == nil:
    fatal "failed to create window"
    glfwTerminate()
    return none(GLFWWindow)

  window.prepareCallbacks()
  window.makeContextCurrent()
  glfwSwapInterval(0) # Disables Vsync

  try:
    discard glInit()
    debug "loaded opengl context",
      renderer = cast[cstring](glGetString(GL_RENDERER)),
      version = cast[cstring](glGetString(GL_VERSION))

  except:

    let
      e = getCurrentException()
      msg = getCurrentExceptionMsg()
   
    fatal "failed loading opengl context",
      exception = repr(e),
      message = msg

    window.destroyWindow()
    return none(GLFWWindow)

  var flags: GLint
  var zero_anger: GLuint = 0
  debug "preparing opengl debug output"
  
  glGetIntegerv(GL_CONTEXT_FLAGS, addr flags)
  if (flags and GL_CONTEXT_FLAG_DEBUG_BIT.GLint) != 0:
    glEnable(GL_DEBUG_OUTPUT)
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS)
    glDebugMessageCallback(on_opengl_message, nil)
    glDebugMessageControl(
      GL_DONT_CARE,
      GL_DONT_CARE,
      GL_DONT_CARE,
      (GLsizei)(0),
      addr zero_anger,
      (GLboolean)(true)
    )
  
  else:
    debug "failed enabling debug output"

  glEnable(GL_DEPTH_TEST)
  glEnable(GL_CULL_FACE)
  glCullFace(GL_BACK)
  some(window)


proc forget*(): void =
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT)
  glClearColor(0.1, 0.2, 0.3, 1.0)


proc update*(window: GLFWWindow): void =
    window.swapBuffers()
    glfwPollEvents()


proc die*(client: Client) =
  client.window.destroyWindow()
  glfwTerminate()


