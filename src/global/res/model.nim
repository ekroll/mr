import strscans
import chronicles
import types


proc obj*(path: string): Model =
  var 
    model: Model
    vertexCount: int = 0
    positions, normals: seq[array[0 .. 2, float32]]
    coords: seq[array[0 .. 1, float32]]

  # OBJ is parsed line by line
  for line in lines path:
    var x, y, z: float

    # Vertex position
    if line.scanf("v $f $f $f",
                      x, y, z):
      let position = [
        x.float32,
        y.float32,
        z.float32
      ]
      
      positions.add(position)
      vertexCount += 1

    # Vertex Normal
    if line.scanf("vn $f $f $f",
                       x, y, z):
      let normal = [
        x.float32,
        y.float32,
        z.float32
      ]

      normals.add(normal)
    
    # Vertex Texture Coordinate
    if line.scanf("vt $f $f",
                       x, y):
      let coord = [
        x.float32, 
        y.float32
      ]

      coords.add(coord)
    
    else: # Stuff I do not handle
      warn "unsupported obj stuff",
        path = path, 
        line = line
  
  try: # Collect everything in a list of vertices
    for i in 0 .. (vertexCount - 1):
      
      let vertex = Vertex(
        position: positions[i],
        normal: normals[i],
        texcoord: coords[i]
      )

      model.vertices.add(vertex)

  except:
    error "malformed obj file, attributes don't match vertex count", file = path

  model.vertexCount = vertexCount.uint32
  return model
