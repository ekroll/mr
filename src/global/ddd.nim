#import chronicles
import kashae
import types

import ddd/[
  continuum,
  matter,
  observer
]

export types

proc begin*(setting: Setting): Client =
  ## Set up OpenGL and create a window
  let window = observer.openWindow(
    setting.name, 
    setting.res[0], 
    setting.res[1]
  )
  
  assert window.isSome()
  setting.toClient(window.get())


proc createBody*(asset: Asset, universe: var Universe) =
  ## Breaks energy conservation
  let body = asset.realize()
  continuum.create(universe, body)


proc isActive*(client: Client): bool =
  return not client.window.windowShouldClose()
  


proc progress*(universe: Universe, client: Client, delta: float): void =
  # Clear last frame
  observer.forget()
  
  # Do physics
  universe.progress(delta)

  # Render the universe
  for body in universe.bodies:
    matter.update(body, client)

  # Display the rendered universe
  observer.update(client.window)


proc destruct*(body: Body) =
  if body.model.isSome():
    let model = body.model.get()
    matter.destroy(model)


proc finish*(universe: Universe, client: Client): void =
  for body in universe.bodies:
    destruct(body)

  observer.die(client)
