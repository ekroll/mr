import std/[
  httpclient,
  strformat,
  strscans,
  strutils,
  parsexml,
  streams,
  json,
  os
]

import types
import chronicles
import res/model
import kashae


proc getJson*(path: string): JsonNode {.cache.} =
  ## Used when parsing JSON manually
  let content = readFile(path)
  let json = parseJson(content)
  doAssert json.kind == JObject
  return json


proc getPath*(key: string): string {.cache.} =
  ## Returns the path to the logging directory in client.json
  let cfg = get_json("./mr.json")
  let path = cfg{key}.getStr()
  return path


proc getLogfile*(filename: string): string =
  ## Returns path to new/old logfile by name
  let dir = getPath("log")
  return dir & filename & ".log"


proc getEndpoint*(key: string): string {.cache.} =
  ## Returns url to endpoint specified in remode_endpoints.json
  let cfg = get_json("./config/endpoints.json")
  let url = cfg{key}.getStr()
  return url


proc downstream*(url: string): string =
  ## "Downloads" to memory for quick processing
  var client = newHttpClient()

  try:
    client.getContent(url)
    
  except:
    "failed"


proc tagExists*(text: string, tag: string): bool {.cache.} =
  ## Checks if a tag exists in a XML document
  var s = newStringStream(text)

  if s == nil:
    quit("could not make stream")

  var x: XmlParser
  open(x, s, "nofile")

  while true:
    x.next()

    case x.kind 
    of xmlElementStart:
      if cmpIgnoreCase(x.elementName, "title") == 0:
        return true

    of xmlEof:
      return false

    else: break

  x.close()
  false


proc parseSettings*(path: string): Setting =
  ## Returns render configuration from json file
  readFile(path).parseJson().to(Setting)


proc parseAsset*(path: string): Body {.cache.} =
  ## Returns Object by name
  echo path
  readfile(path).parseJson().to(Body)


proc loadModel*(path: string): Option[Model] =
  var data: Option[Model]

  try:
    var parsed: Model
    let (_, _, fileExtension) = os.splitFile(path)

    case fileExtension:
      of ".obj": parsed = model.obj(path)
      else: warn "unsupported model filetype", 
        file = path,
        filetype = fileExtension
    
    debug "model data", parsed = parsed
    data = some(parsed)

  except CatchableError:
    error "failed to parse model", model = path
    data = none(Model)

  return data